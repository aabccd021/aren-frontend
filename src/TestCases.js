import React from "react";
import AceEditor from 'react-ace'
import Grid from '@material-ui/core/Grid';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

import 'brace/theme/github'; // this is needed as the default theme
import "brace/mode/jsx";
import "brace/ext/searchbox";

const styles = theme => ({
    root: {
        width: '100%',
        marginTop: '20px',
    },
    heading: {
        fontSize: theme.typography.pxToRem(15),
        fontWeight: theme.typography.fontWeightRegular,
    },
});

function TestCases(props) {
    const { classes } = props;
    return (
        <div className={classes.root}>
            <ExpansionPanel defaultExpanded={!props.pass}>
                <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                    <Typography variant="h5" component="h3">
                        #{props.number + 1}
                    </Typography>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails>
                    <div className="papera">
                        <br></br>
                        <div>
                            <Typography>
                                Contributor : {props.contributor}
                            </Typography>
                        </div>
                        <br></br>

                        <div>
                            <Grid container spacing={24}>
                               <Grid item xs={6}>

                        <Typography>
                            Problem:
            </Typography>
                                </Grid>
                                <Grid item xs={6}>

                                    <Typography>
                                        Expected:
            </Typography>
                                </Grid>
                                <Grid item xs={6}>
                                    <div className="border">
                                        <AceEditor
                                            theme="github"
                                            editorProps={{ $blockScrolling: true }}
                                            value={props.problem}
                                            height="250px"
                                            width="100%"
                                            mode="text"
                                            readOnly={true}
                                            fontSize={17}
                                            highlightActiveLine={false}
                                        />,

</div>
                                </Grid>
                                <Grid item xs={6}>
                            <div className="border">
                                        <AceEditor
                                            theme="github"
                                            editorProps={{ $blockScrolling: true }}
                                            value={props.answer}
                                            height="250px"
                                            width="100%"
                                            mode="text"
                                            readOnly={true}
                                            fontSize={17}
                                            highlightActiveLine={false}
                                        />,

                            </div>

 
 </Grid>
                            </Grid>


                            
                        </div>
                    </div>

                </ExpansionPanelDetails>
            </ExpansionPanel>
        </div>
    );
}

TestCases.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(TestCases);

